from random import random


print("running checks...")
correct_values = {"pi":3.141592654, "log(2)":0.693147181, "e":2.718281828, "sqrt(2)":1.414213562}
# Pi
runs = 0
max_runs = 10000
ins = 0
while runs < max_runs:
    runs += 1
    x = random()
    y = random()
    if x*x + y*y <= 1:
        ins += 1
pi = ins/max_runs * 4
print("pi = ", str(pi), ", % error: ", str((pi - correct_values["pi"])/correct_values["pi"]))

# Natural Log 2
def log(z, max_runs = 10000):
    runs = 0
    ins = 0
    while runs < max_runs:
        runs += 1
        x = 1 + (z - 1) * random()
        y = random()
        if y <= 1.0/x:
            ins += 1
    return ins/max_runs * (z-1)

log_2 = log(2)
print("ln(2) = ", log_2, ", % error: ", (log_2 - correct_values["log(2)"])/correct_values["log(2)"])

# exponential
def exp(z, max_runs = 1000, guess = 0):
    runs = 0
    while runs < max_runs:
        runs += 1
        w = log(guess)
        if w < z:
            guess += (z - w)
    return guess

e = exp(1)
print("e =", e, ", % error:", (e - correct_values["e"])/correct_values["e"])

# power function a^b
def pwr(a, b):
    return exp(b*log(a))

sqrt_2 = pwr(2, .5)
print("sqrt(2) = ", sqrt_2, ", % error: ", (sqrt_2 - correct_values["sqrt(2)"])/correct_values["sqrt(2)"])

print("done")


