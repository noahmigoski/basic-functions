Basic Functions
===============

This is a short exercise to derive functions like e^x, ln(x), a^b, and so on using only addition, multiplication, and random numbers.

Here is a sample output for calculating some well known constants:

`running checks...`

`pi =  3.1404 , % error:  -0.0003796335589470777`

`ln(2) =  0.6976 , % error:  0.00642405988519782`

`e = 2.7923919093782597 , % error: 0.027263575327208422`

`sqrt(2) =  1.4234548151635515 , % error:  0.006534552780332841`

`done`

You can also load the script in order to access the functions for any value.
